<?php

use Dotenv\Dotenv;
use Lmfriends\LmfdsCafe24\Cafe24App;

$rootPath = __DIR__ . '/../';
require_once $rootPath . 'vendor/autoload.php';

$dotenv = Dotenv::createImmutable($rootPath);
$dotenv->load();

$app = new Cafe24App('lmfdsmall', 1);
