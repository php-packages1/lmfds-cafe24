# Cafe24 Middleware class v1.0
- Cafe24TokenRepository  
- Cafe24StoreRepository  
  
- 할인 앱 서비스  
- 배송 앱 서비스  
  

## 설치
composer.json에 다음과 같은 항목을 추가하고 composer update

```
"require": {
    "lmfriends/lmfds-cafe24" : "~1.0.5"
},
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/php-packages1/lmfds-cafe24"
    }
]
```


## Cafe24 환경 설정
.env
```
CAFE24_APP_NAME: cafe24App
CAFE24_APP_URI: https://lmfriends.app/xxxx/app
CAFE24_CLIENT_ID: xxxxxx
CAFE24_CLIENT_SECRET: xxxxxx
CAFE24_SERVICE_KEY: xxxxxx
CAFE24_APP_SCOPE: mall.write_application, mall.read_application, mall.write_order, mall.read_order, ...
```
