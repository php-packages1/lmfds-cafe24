<?php

/**
 * Cafe24AppManage - 카페24 앱 관리 링크
 *
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsCafe24;

class Cafe24AppManage
{
  private $authorizeCode;
  private $state;
  private $appManageUrl;

  public function __construct($authorizeCode, $state, $appManageUrl = null)
  {
    $this->authorizeCode = $authorizeCode;
    $this->state = $state;
    $this->appManageUrl = $appManageUrl;
  }

  public function run()
  {
    $arrStatue = json_decode(base64_decode($this->state), true);

    $cafe24Service = new Cafe24Service;
    $accessToken = $cafe24Service->authentication($this->authorizeCode, $arrStatue);

    if ($accessToken && $accessToken != 'fail')
      $cafe24Service->saveStore($arrStatue['mall_id'], $arrStatue['shop_no'], $accessToken);

    $appManageUrl = $this->appManageUrl == null
      ? $_ENV['CAFE24_APP_URI'] . '/admin/?mall=' . $arrStatue['mall_id'] . '|' . urlencode(base64_encode($accessToken))
      : $this->appManageUrl;
    header('Location: ' . $appManageUrl);
  }
}
