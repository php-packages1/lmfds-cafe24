<?php

/**
 * AuthorityRepository - API 권한 저장소
 *
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsCafe24\Repositories;

use Lmfriends\LmfdsFoundation\Model;

class AuthorityRepository extends Model
{
  public function __construct($env, $tableName = 'tb_authorities')
  {
    parent::__construct($env, $tableName);
  }

  public function findByMallId($mallId, $code)
  {
    $condition = "WHERE mall_id = '$mallId' and code = '$code'";
    $sql = "SELECT mall_id FROM {$this->_tableName} $condition";
    $result = $this->queryExecute($sql);
    if (count($result) > 0) return $result[0];
    else return false;
  }

  public function save($mallId, $code = 'lmfdsmall@1234')
  {
    if ($this->findByMallId($mallId, $code) !== false) return false;

    $sql = "INSERT INTO {$this->_tableName} (mall_id, code) VALUES ('$mallId', '$code')";
    return $this->queryExecute($sql);
  }
}
