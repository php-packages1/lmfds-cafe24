<?php

/**
 * Cafe24StoreRepository - 카페24 상점 정보 저장소
 *
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsCafe24\Repositories;

use Lmfriends\LmfdsFoundation\Model;

class Cafe24StoreRepository extends Model
{
  public function __construct($env, $tableName = 'cafe24_stores')
  {
    parent::__construct($env, $tableName);
  }

  public function save($data)
  {
    $store = $data['store'];
    $mall_id = $store['mall_id'];
    $shop_no = $store['shop_no'];
    $client_id = $data['CAFE24_CLIENT_ID'];
    $company_name = $store['company_name'];
    $phone = $store['phone'];
    $email = $store['email'];
    $mall_url = $store['mall_url'];
    $payload = json_encode($store, JSON_UNESCAPED_UNICODE);
    $updated_at = date('Y-m-d H:i:s');
    $sql = "INSERT INTO {$this->_tableName} (mall_id, shop_no, client_id, company_name, phone, email, mall_url, payload, updated_at)
      VALUES ('$mall_id', $shop_no, '$client_id', '$company_name', '$phone', '$email', '$mall_url', '$payload', '$updated_at')
      ON DUPLICATE KEY UPDATE
      company_name='$company_name', phone='$phone', email='$email', mall_url='$mall_url', payload='$payload', updated_at='$updated_at'";

    return $this->queryExecute($sql);
  }

  public function read($mallId, $shopNo, $clientId)
  {
    $condition = "WHERE mall_id = '$mallId' AND shop_no = $shopNo AND client_id = '$clientId'";
    $sql = "SELECT * FROM {$this->_tableName} $condition";
    $record = $this->queryExecute($sql);
    return isset($record[0]) ? $record[0] : null;
  }
}
