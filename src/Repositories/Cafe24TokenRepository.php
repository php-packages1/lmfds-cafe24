<?php

/**
 * Cafe24TokenRepository - 카페24 엑세스 토큰 저장소
 *
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsCafe24\Repositories;

use Lmfriends\LmfdsFoundation\Model;

class Cafe24TokenRepository extends Model
{
  public function __construct($env, $tableName = 'cafe24_tokens')
  {
    parent::__construct($env, $tableName);
  }

  public function save($data)
  {
    $mall_id = $data['mall_id'];
    $shop_no = $data['shop_no'];
    $client_id = $data['CAFE24_CLIENT_ID'];
    $access_token = $data['access_token'];
    $expires_at = $data['expires_at'];
    $refresh_token = $data['refresh_token'];
    $refresh_token_expires_at = $data['refresh_token_expires_at'];
    $payload = json_encode($data, JSON_UNESCAPED_UNICODE);
    $updated_at = date('Y-m-d H:i:s');
    $sql = "INSERT INTO {$this->_tableName} (mall_id, shop_no, client_id, access_token, expires_at, refresh_token, refresh_token_expires_at, payload, created_at, updated_at)
      VALUES ('$mall_id', $shop_no, '$client_id', '$access_token', '$expires_at', '$refresh_token', '$refresh_token_expires_at', '$payload', '$updated_at', '$updated_at')
      ON DUPLICATE KEY UPDATE
      access_token='$access_token', expires_at='$expires_at', refresh_token='$refresh_token', refresh_token_expires_at='$refresh_token_expires_at', payload='$payload', updated_at='$updated_at'";

    return $this->queryExecute($sql);
  }

  public function read($mallId, $shopNo, $clientId)
  {
    $condition = "WHERE mall_id = '$mallId' AND shop_no = $shopNo AND client_id = '$clientId'";
    $sql = "SELECT * FROM {$this->_tableName} $condition";
    return $this->queryExecute($sql);
  }
}
