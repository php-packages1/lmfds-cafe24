<?php

/**
 * Cafe24SaleService - 카페24 할인 앱 서비스
 * 
 * 할인 앱 스펙
 *    discount_unit 	            적용 범위(I:수량무관, U:수량비례)
 *    use_coupon_simultaneously 	앱 할인 적용 유무 (F인경우 앱 할인이 적용되어 쿠폰할인이 적용되지 않음)
 *    type 		                    할인 타입(P:상품, O:주문)
 *    value_type 	                할인 적용 타입(W:정액, P:정률)
 *
 * 
 * discountInfo
 *    no
 *    name
 *    price
 *    type
 *    discount_unit
 *    value_type
 * 
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsCafe24;

class Cafe24SaleService
{
  /*
  * no
  * name
  * price
  * type
  * discount_unit
  * value_type
  */
  private $clientId;
  private $serviceKey;
  private $discountInfo;
  public function __construct($clientId, $serviceKey, $discountInfo)
  {
    $this->clientId = $clientId;
    $this->serviceKey = $serviceKey;
    $this->discountInfo = $discountInfo;
  }

  public function orderSale($orderInfo)
  {
    $traceNo = $this->makeTraceNo();

    $respOrderMap = $this->doSale($orderInfo, $traceNo);
    $respOrderMap['guest_key'] = md5($orderInfo['member_id']);

    $respOrderMap['hmac'] = $this->getHmac($respOrderMap);
    unset($respOrderMap['guest_key']);

    return json_encode($respOrderMap, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
  }

  protected function makeTraceNo()
  {
    $traceNo = date('YmdHis') . substr(gettimeofday()['usec'], 0, 3) . $this->generateTokenid(6);
    return $traceNo;
  }

  protected function doSale($orderInfo, $traceNo)
  {
    $discountNo = $this->discountInfo['no'];
    $discountName = $this->discountInfo['name'];
    $discount = $this->discountInfo['price'];
    $type = $this->discountInfo['type'];
    $discountUnit = $this->discountInfo['discount_unit'];
    $valueType = $this->discountInfo['value_type'];

    $app_product_discount_info = [
      [
        "no" => $discountNo,
        "price" => $discount,
        "discount_unit" => $discountUnit
      ]
    ];

    $product_discount = [];
    foreach ($orderInfo['product'] as $index => $product) {
      $info = [
        "basket_product_no" => $product['basket_product_no'],
        "product_no" => $product['product_no'],
        "variant_code" => $product['variant_code'],
        "quantity" => $product['quantity'],
        "price" => $product['price'],
        "option_price" => $product['option_price'],
        "quantity_based_discount" => $product['quantity_based_discount'],
        "non_quantity_based_discount" => $product['non_quantity_based_discount'],
        "app_quantity_based_discount" => 0,
        "app_non_quantity_based_discount" => $discount,
        "app_product_discount_info" => $app_product_discount_info,
      ];
      if ($index > 0) {
        $info['app_non_quantity_based_discount'] = 0;
        $info['app_product_discount_info'] = [];
      }

      array_push($product_discount, $info);
    }

    $order_discount = [];

    $app_discount_info = [
      [
        "no" => $discountNo,
        "type" => $type,
        "name" => $discountName,
        "icon" => "http://placehold.it/32x32",
        "config" => [
          "value" => $discount,
          "value_type" => $valueType,
          "discount_unit" => $discountUnit
        ]
      ]
    ];

    $data = [
      'mall_id' => $orderInfo['mall_id'],
      'shop_no' => $orderInfo['shop_no'],
      'member_id' => $orderInfo['member_id'],
      'member_group_no' => $orderInfo['member_group_no'],
      'time' => $orderInfo['time'],
      'product_discount' => $product_discount,
      'order_discount' => $order_discount,
      'app_discount_info' => $app_discount_info,
      'trace_no' => $traceNo,
      'app_key' => $this->clientId,
    ];

    return $data;
  }

  protected function getHmac($respOrderMap)
  {
    $plainText = json_encode($respOrderMap, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    $hmac = base64_encode(hash_hmac('sha256', $plainText, $this->serviceKey, true));
    return $hmac;
  }

  protected function generateTokenid($depth)
  {
    $key = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  
    $token = '';
    for ($i = 0; $i < $depth; $i++) {
      $token .= $key[rand(0, 61)];
    }
  
    return $token;
  }
}
