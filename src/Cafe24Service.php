<?php

/**
 * Cafe24Service - 카페24 서비스
 *
 * Version 1.0.2
 */


namespace Lmfriends\LmfdsCafe24;

use Lmfriends\LmfdsCafe24\Repositories\AuthorityRepository;
use Lmfriends\LmfdsCafe24\Repositories\Cafe24StoreRepository;
use Lmfriends\LmfdsCafe24\Repositories\Cafe24TokenRepository;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Cafe24Service
{
  private $logger;
  private $dbInfo;

  public function __construct()
  {
    $path = explode('/vendor', __DIR__)[0] . '/logs/cafe24-service.log';
    $this->logger = new Logger('Cafe24Service');
    $this->logger->pushHandler(new StreamHandler($path));

    $this->dbInfo = [
      'host' => $_ENV['DB_HOST'],
      'dbname' => $_ENV['DB_DATABASE'],
      'username' => $_ENV['DB_USERNAME'],
      'password' => $_ENV['DB_PASSWORD'],
      'charset' => isset($_ENV['DB_CHARSET']) ? $_ENV['DB_CHARSET'] : 'utf8mb4'
    ];
  }

  public function authentication($authorizeCode, $params)
  {
    $formBody = array(
      'grant_type' => 'authorization_code',
      'code' => $authorizeCode,
      'redirect_uri' => $_ENV["CAFE24_APP_URI"] . "/manage"
    );

    $res = $this->requestWithAuthorization($params["mall_id"], $formBody);
    if ($res) $res = $this->saveToken($res);
    else return "fail";

    return $res;
  }

  public function getAccessTokenByRefreshToken($mallId, $shopNo, $refreshToken = null)
  {
    if (!$refreshToken) {
      $token = $this->readToken($mallId, $shopNo);
      $refreshToken = $token[0]['refresh_token'];
      $refreshTokenExpiresAt = $token[0]['refresh_token_expires_at'];
      if ($this->isExpired($refreshTokenExpiresAt)) return "";
    }

    $formBody = array(
      'grant_type' => 'refresh_token',
      'refresh_token' => $refreshToken
    );

    $res = $this->requestWithAuthorization($mallId, $formBody);
    if ($res) return $this->saveToken($res);
    else {
      $this->logger->error('getAccessTokenByRefreshToken', ['extra' => ['response' => $res]]);
      return "";
    }
  }

  public function rest($mallId, $shopNo, $accessToken = null)
  {
    if ($accessToken == null)
      $accessToken = $this->getAccessToken($mallId, $shopNo);
    return new Cafe24REST($mallId, $shopNo, $accessToken);
  }

  public function getAccessToken($mallId, $shopNo)
  {
    $token = $this->readToken($mallId, $shopNo);
    if (!isset($token[0])) return "";

    $accessToken = $token[0]['access_token'];
    $expiresAt = $token[0]['expires_at'];
    $refreshToken = $token[0]['refresh_token'];

    if ($this->isExpired($expiresAt))
      $accessToken = $this->getAccessTokenByRefreshToken($mallId, $shopNo, $refreshToken);
    return $accessToken;
  }

  public function saveStore($mallId, $shopNo, $accessToken = null)
  {
    $res = $this->rest($mallId, $shopNo, $accessToken)->store()->request('GET');

    $repository = new Cafe24StoreRepository($this->dbInfo);
    $result = $repository->save(array_merge(['CAFE24_CLIENT_ID' => $_ENV['CAFE24_CLIENT_ID']], $res));
    if (isset($result['error']))
      $this->logger->error('Cafe24StoreRepository save', ['extra' => ['result' => $result]]);

    $repository = new AuthorityRepository($this->dbInfo);
    $result = $repository->save($mallId);
    if (isset($result['error']))
      $this->logger->error('AuthorityRepository save', ['extra' => ['result' => $result]]);
  }

  protected function isExpired($expiresAt)
  {
    $thresholdSeconds = 30 * 60;
    $date1 = date_create(date('Y-m-d H:i:s'))->getTimestamp();
    $date2 = date_create($expiresAt)->getTimestamp();
    $diff = $date2 - $date1;
    if ($diff > $thresholdSeconds) return false;
    return true;
  }

  protected function requestWithAuthorization($mallId, $formBody)
  {
    $sEndPointUrl = "https://$mallId.cafe24api.com/api/v2/oauth/token";
    $preparedAuthorization = $_ENV['CAFE24_CLIENT_ID'] . ':' . $_ENV['CAFE24_CLIENT_SECRET'];
    $strAuthorization = 'Authorization: Basic ' . base64_encode($preparedAuthorization);

    // 발급 받은 인증 코드를 사용하여 실제로 API를 호출할 수 있는 사용자 토큰(Access Token, Refresh Token) 요청
    $oCurl = curl_init();
    $option = array(
      CURLOPT_URL => $sEndPointUrl,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => http_build_query($formBody),
      CURLOPT_HTTPHEADER  => array(
        $strAuthorization,
        'Content-Type: application/x-www-form-urlencoded'
      )
    );
    curl_setopt_array($oCurl, $option);
    $sResponse = curl_exec($oCurl);
    if (curl_errno($oCurl)) {
      $this->logger->error('requestWithAuthorization: ' . curl_error($oCurl), ['extra' => ['option' => $option]]);
      $sResponse = null;
    }

    curl_close($oCurl);
    return $sResponse ? json_decode($sResponse, true) : false;
  }

  protected function saveToken($token)
  {
    if (!$token || !isset($token['mall_id'])) return '';

    $cafe24TokenRepository = new Cafe24TokenRepository($this->dbInfo);
    $result = $cafe24TokenRepository->save(array_merge(['CAFE24_CLIENT_ID' => $_ENV['CAFE24_CLIENT_ID']], $token));
    if (isset($result['error']))
      $this->logger->error('Cafe24TokenRepository save', ['extra' => ['result' => $result]]);

    $access_token = isset($result['success']) ? $token['access_token'] : '';
    return $access_token;
  }

  protected function readToken($mallId, $shopNo)
  {
    $clientId = $_ENV['CAFE24_CLIENT_ID'];
    $cafe24TokenRepository = new Cafe24TokenRepository($this->dbInfo);
    $result = $cafe24TokenRepository->read($mallId, $shopNo, $clientId);
    if (isset($result['error']))
      $this->logger->error('Cafe24TokenRepository read', ['extra' => ['result' => $result]]);

    return $result;
  }
}
