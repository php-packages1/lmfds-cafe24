<?php

/**
 * Cafe24ShippingFeeService - 카페24 배송 앱 서비스
 * 
 * 배송 앱 스펙
 *  shippingMethod: 배송비 로직 함수
 *  return value
 *    [
 *      id,         // Mixed (Integer or String) Type (배송방법 식별자)
 *      title,      // String Type (배송방법 타이틀)
 *      ship_fee    // Float Type (배송비)
 *      description // String Type (배송비설명)
 *    ]
 * 
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsCafe24;

class Cafe24ShippingFeeService
{
  /*
  * client_id
  * client_name
  * mall_id
  * shop_no
  * time
  * trace_id
  * request
  * shipping_method
  */
  private $clientId;
  private $serviceKey;
  private $shippingInfo;
  private $shippingMethod;
  public function __construct($clientId, $serviceKey, $shippingInfo, $shippingMethod)
  {
    $this->clientId = $clientId;
    $this->serviceKey = $serviceKey;
    $this->shippingInfo = $shippingInfo;
    $this->shippingMethod = $shippingMethod;
  }

  public function shippingFee()
  {
    $traceNo = $this->makeTraceNo();

    $respOrderMap = $this->calcFee($traceNo);

    $respOrderMap['hmac'] = $this->getHmac($respOrderMap);

    return json_encode($respOrderMap, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
  }

  protected function makeTraceNo()
  {
    $traceNo = date('YmdHis') . substr(gettimeofday()['usec'], 0, 3) . $this->generateTokenid(6);
    return $traceNo;
  }

  protected function calcFee($traceNo)
  {
    $shipping_method = call_user_func($this->shippingMethod, $this->shippingInfo);

    $data = [
      'client_id' => $this->clientId,
      'client_name' => $_ENV['CAFE24_APP_NAME'],
      'mall_id' => $this->shippingInfo['mall_id'],
      'shop_no' => $this->shippingInfo['shop_no'],
      'time' => time(),
      'trace_no' => $traceNo,
      'request' => $this->shippingInfo,
      'shipping_method' => [$shipping_method],
    ];

    return $data;
  }

  protected function getHmac($respOrderMap)
  {
    $plainText = json_encode($respOrderMap, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    $hmac = base64_encode(hash_hmac('sha256', $plainText, $this->serviceKey, true));
    return $hmac;
  }

  protected function generateTokenid($depth)
  {
    $key = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    $token = '';
    for ($i = 0; $i < $depth; $i++) {
      $token .= $key[rand(0, 61)];
    }

    return $token;
  }
}
