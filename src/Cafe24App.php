<?php

/**
 * Cafe24App - 카페24 앱 인증 링크 (앱 진입)
 *
 * Version 1.0.1
 */


namespace Lmfriends\LmfdsCafe24;

class Cafe24App
{
  private $mallId;
  private $shopNo;
  private $authCodeReceiveUrl;

  public function __construct($mallId, $shopNo, $authCodeReceiveUrl = null)
  {
    $this->mallId = $mallId;
    $this->shopNo = $shopNo;
    $this->authCodeReceiveUrl = $authCodeReceiveUrl == null
      ? $_ENV['CAFE24_APP_URI'] . '/manage'
      : $authCodeReceiveUrl;
  }

  public function run()
  {
    $aState = array('mall_id' => $this->mallId, 'shop_no' => $this->shopNo);

    $aRequestData = array(
      'response_type' => 'code',
      'client_id' => $_ENV['CAFE24_CLIENT_ID'],
      'state' => base64_encode(json_encode($aState)),
      'redirect_uri' => $this->authCodeReceiveUrl,
      'scope' => $_ENV['CAFE24_APP_SCOPE'],
    );

    $sAuthCodeRequestUrl = "https://{$this->mallId}.cafe24api.com/api/v2/oauth/authorize?";
    $sUrl = $sAuthCodeRequestUrl . http_build_query($aRequestData);

    header('Location: ' . $sUrl);
  }
}
