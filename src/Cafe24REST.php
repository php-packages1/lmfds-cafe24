<?php

/**
 * Cafe24REST - 카페24 REST API
 *
 * Version 1.0.5
 */


namespace Lmfriends\LmfdsCafe24;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Cafe24REST
{
  private $logger;

  private $mallId;
  private $shopNo;
  private $accessToken;
  private $baseUrl;
  private $URI;
  private $isMultiRequest;
  private $postData;

  public function __construct($mallId, $shopNo, $accessToken)
  {
    $path = explode('/vendor', __DIR__)[0] . '/logs/cafe24-rest.log';
    $this->logger = new Logger('Cafe24REST');
    $this->logger->pushHandler(new StreamHandler($path));

    $this->mallId = $mallId;
    $this->shopNo = $shopNo;
    $this->accessToken = $accessToken;
    $this->baseUrl = "https://{$mallId}.cafe24api.com/api/v2/admin";
    return $this;
  }

  public function http_parse_headers($raw_headers)
  {
    $headers = array();
    $key = '';

    foreach (explode("\n", $raw_headers) as $i => $h) {
      $h = explode(':', $h, 2);

      if (isset($h[1])) {
        if (!isset($headers[$h[0]]))
          $headers[$h[0]] = trim($h[1]);
        elseif (is_array($headers[$h[0]])) {
          $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1])));
        } else {
          $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1])));
        }

        $key = $h[0];
      } else {
        if (substr($h[0], 0, 1) == "\t")
          $headers[$key] .= "\r\n\t" . trim($h[0]);
        elseif (!$key)
          $headers[0] = trim($h[0]);
        trim($h[0]);
      }
    }

    return $headers;
  }

  public function request($requestType)
  {
    $reqCurl = curl_init();
    $reqOption = [
      CURLOPT_URL => $this->URI,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => true,
      CURLOPT_CUSTOMREQUEST => $requestType,
      CURLOPT_HTTPHEADER => [
        'Authorization: Bearer ' . $this->accessToken,
        'Content-Type: application/json'
      ]
    ];
    if ($requestType == 'POST' || $requestType == 'PUT') {
      $reqOption[CURLOPT_POSTFIELDS] = json_encode([
        'shop_no' => $this->shopNo,
        ($this->isMultiRequest ? 'requests' : 'request') => $this->postData
      ]);
    }
    curl_setopt_array($reqCurl, $reqOption);

    $sResponse = curl_exec($reqCurl);
    if (curl_errno($reqCurl)) {
      $this->logger->error('request: ' . curl_error($reqCurl), ['extra' => ['option' => $reqOption]]);
      $body = null;
    } else {
      $header_size = curl_getinfo($reqCurl, CURLINFO_HEADER_SIZE);
      $header = substr($sResponse, 0, $header_size);
      $body = substr($sResponse, $header_size);
      $responseHeaders = $this->http_parse_headers($header);
    }

    curl_close($reqCurl);
    $response = $body ? json_decode($body, true) : false;
    if ($response !== false) {
      if (isset($responseHeaders['X-Trace_ID']))
        $response['X-Trace_ID'] = $responseHeaders['X-Trace_ID'];
      else if (isset($responseHeaders['x-trace_id']))
        $response['X-Trace_ID'] = $responseHeaders['x-trace_id'];

      if (isset($responseHeaders['X-Api-Call-Limit']))
        $response['X-Api-Call-Limit'] = $responseHeaders['X-Api-Call-Limit'];
      else if (isset($responseHeaders['x-api-call-limit']))
        $response['X-Api-Call-Limit'] = $responseHeaders['x-api-call-limit'];

      if (isset($response['error']))
        $this->logger->error('response: ', ['extra' => ['response' => $response]]);
    }
    return $response;
  }

  public function app()
  {
    $this->URI = $this->baseUrl . "/apps";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function store()
  {
    $this->URI = $this->baseUrl . "/store?shop_no={$this->shopNo}";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function searchOrder($option)
  {
    $this->URI = $this->baseUrl . "/orders?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function countOrder($option)
  {
    $this->URI = $this->baseUrl . "/orders/count?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function order($orderId, $embed)
  {
    $this->URI = $this->baseUrl . "/orders/$orderId?shop_no={$this->shopNo}";
    if ($embed) $this->URI .= "&embed={$embed}";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function updateOrderStatus($orderId, $status, $orderItemCode)
  {
    $this->URI = $this->baseUrl . "/orders/$orderId";
    $this->isMultiRequest = false;
    $this->postData = [
      'process_status' => $status,
      'order_item_code' => $orderItemCode,
    ];
    return $this;
  }

  // {
  //   "name": "John Doe",
  //   "cellphone": "010-0000-0000",
  //   "shipping_message": "Sample shipping message",
  //   "zipcode": "06258",
  //   "address1": "Sindaebang dong Dongjak-gu, Seoul, Republic of Korea",
  //   "address2": "Professional Construction Hall ",
  //   "shipping_code": "D-20170710-0000013-00",
  // }
  public function updateOrderRecipients($orderId, $params)
  {
    $this->URI = $this->baseUrl . "/orders/$orderId/receivers";
    $this->isMultiRequest = true;
    $this->postData = $params;
    return $this;
  }

  // {
  //   "status": "canceled",
  //   "reason": "Damaged product",
  //   "claim_reason_type": "I",
  //   "add_memo_too": "T",
  //   "recover_coupon": "T",
  //   "recover_coupon_no": [
  //     6073442924700000008,
  //     6073442924700000009
  //   ],
  //   "recover_inventory": "T",
  //   "refund_method_code": [
  //     "F",
  //     "T"
  //   ],
  //   "refund_bank_code": "bank_82",
  //   "refund_bank_account_no": "000000111111",
  //   "refund_bank_account_holder": "John Doe",
  //   "payment_gateway_cancel": "F",
  //   "items": [
  //     {
  //       "order_item_code": "20190805-0000011-01",
  //       "quantity": 4
  //     },
  //     {
  //       "order_item_code": "20190805-0000011-02",
  //       "quantity": 4
  //     }
  //   ]
  // }
  public function createOrderCancellation($orderId, $params)
  {
    $this->URI = $this->baseUrl . "/orders/$orderId/cancellation";
    $this->isMultiRequest = false;
    $this->postData = $params;
    return $this;
  }

  public function createShipping($orderId, $orderItemCode, $trackingNo, $shippingCompanyCode, $status)
  {
    $this->URI = $this->baseUrl . "/orders/$orderId/shipments";
    $this->isMultiRequest = false;
    $this->postData = [
      'tracking_no' => $trackingNo,
      'shipping_company_code' => $shippingCompanyCode,
      'status' => $status,
      'order_item_code' => [$orderItemCode]
    ];
    return $this;
  }

  public function updateShipping($orderId, $shippingCode, $status)
  {
    $this->URI = $this->baseUrl . "/orders/$orderId/shipments/$shippingCode";
    $this->isMultiRequest = false;
    $this->postData = [
      'status' => $status
    ];
    return $this;
  }

  public function deleteShipping($orderId, $shippingCode)
  {
    $this->URI = $this->baseUrl . "/orders/$orderId/shipments/$shippingCode";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function searchCustomer($option)
  {
    $this->URI = $this->baseUrl . "/customersprivacy?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function countCustomer($option)
  {
    $this->URI = $this->baseUrl . "/customersprivacy/count?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function customersPrivacy($memberId)
  {
    $this->URI = $this->baseUrl . "/customersprivacy/$memberId";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function updateCustomersPrivacy($memberId, $updateData)
  {
    $this->URI = $this->baseUrl . "/customersprivacy/$memberId";
    $this->isMultiRequest = false;
    $this->postData = $updateData;
    return $this;
  }

  public function customersMemo($memberId)
  {
    $this->URI = $this->baseUrl . "/customers/$memberId/memos";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function createCustomersMemo($memberId, $author_id, $memo, $important_flag)
  {
    $this->URI = $this->baseUrl . "/customers/$memberId/memos";
    $this->isMultiRequest = false;
    $this->postData = [
      'author_id' => $author_id,
      'memo' => $memo,
      'important_flag' => $important_flag
    ];
    return $this;
  }

  public function searchCustomerGroups($option)
  {
    $this->URI = $this->baseUrl . "/customergroups?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function product($productNo, $embed)
  {
    $this->URI = $this->baseUrl . "/products/$productNo?shop_no={$this->shopNo}";
    if ($embed) $this->URI .= "&embed={$embed}";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function searchProduct($option)
  {
    $this->URI = $this->baseUrl . "/products?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function countProduct($option)
  {
    $this->URI = $this->baseUrl . "/products/count?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function searchSupplier($option)
  {
    $this->URI = $this->baseUrl . "/suppliers?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function countSupplier($option)
  {
    $this->URI = $this->baseUrl . "/suppliers/count?";
    if (is_array($option)) $this->URI .= http_build_query($option);
    else $this->URI .= $option;
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function createAppstoreOrder($orderName, $orderAmount, $returnUrl, $automaticPayment = 'F')
  {
    $this->URI = $this->baseUrl . "/appstore/orders";
    $this->isMultiRequest = false;
    $this->postData = [
      'order_name' => $orderName,
      'order_amount' => $orderAmount,
      'return_url' => $returnUrl,
      'automatic_payment' => $automaticPayment,
    ];
    return $this;
  }

  public function appstoreOrder($orderId)
  {
    $this->URI = $this->baseUrl . "/appstore/orders/$orderId";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }

  public function createScripttag($option)
  {
    $this->URI = $this->baseUrl . "/scripttags";
    $this->isMultiRequest = false;
    $this->postData = $option;
    return $this;
  }

  public function updateScripttag($scriptNo, $option)
  {
    $this->URI = $this->baseUrl . "/scripttags/$scriptNo";
    $this->isMultiRequest = false;
    $this->postData = $option;
    return $this;
  }

  public function deleteScripttag($scriptNo)
  {
    $this->URI = $this->baseUrl . "/scripttags/$scriptNo";
    $this->isMultiRequest = false;
    $this->postData = null;
    return $this;
  }
}
