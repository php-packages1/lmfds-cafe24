CREATE TABLE IF NOT EXISTS `cafe24_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '쇼핑몰 아이디',
  `shop_no` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `client_id` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `refresh_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refresh_token_expires_at` timestamp NULL DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci COMMENT '응답 받은 원본 데이터',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_mall_id_shop_no_client_id` (`mall_id`,`shop_no`,`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
