CREATE TABLE `tb_authorities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '쇼핑몰 아이디',
  `code` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
